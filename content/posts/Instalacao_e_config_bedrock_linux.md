---
title: "Instalação e configuração do BedRock Linux"
date: 2021-04-09T20:34:10-03:00
draft: false
author: Deriki
tags: ["Linux Geral", "Intermediário"]
cover: https://media.discordapp.net/attachments/829041730900721715/830461383300481105/unknown.png
---
Olá! Este é o guia para instalar o BedRock Linux em cima da distribuição que você está usando!

## Arquivos Para Download
Primeiro, baixe o script [aqui](https://raw.githubusercontent.com/bedrocklinux/bedrocklinux-userland/0.7/releases) de acordo com a arquitetura do seu processador.
## Instalação
Abra seu gerenciador de arquivos, e entre na pasta onde está o script (Geralmente /home/$USER/downloads) e clique com o botão direito do mouse. 
Deve aparecer algo assim:

![Deve aparecer algo assim](https://cdn.discordapp.com/attachments/829041782603120650/830143275456397342/propriedades.png) 

Então clique em "**Abrir terminal**".
No terminal, o shell deve estar parecido com este:

![](https://cdn.discordapp.com/attachments/829041782603120650/830144743819575296/shell.png)

Então, digite:
> sudo sh ./bedrock-linux-`versão`-`arquitetura`.sh --hijack

O script do bedrock linux deve começar a rodar, e uma tela assim deve aparecer:
![enter image description here](https://cdn.discordapp.com/attachments/829041782603120650/830168661154857091/bedrock-script.png)
Se aparecer, escreva:
> Not Reversible!

E a instalação irá começar. Após o fim, reinicie o computador e abra o terminal denovo. Quando entrar digite
> brl fetch --list

Para receber a lista das distros disponíveis.
No meu caso apareceram essas distros:

![enter image description here](https://cdn.discordapp.com/attachments/829041782603120650/830176304267526174/unknown.png)

E eu escolhi o Arch Linux por causa do package manager pacman, e o Void Linux por causa do init system Runit, mas você pode escolher as distros de sua preferência.
       
Para instalar uma distro, basta digitar:

> brl fetch -n tut-distro distro

Só que no lugar de distro, coloque o nome da distribuição que você deseja!
## Aplicando as configurações
Se você só quer o package manager da distribuição, é só reiniciar e pronto. Mas se você também quer trocar o init system continue lendo o artigo
## Mudando o init system
Para mudar o init system é bem simples, entre em um terminal e digite
> cd /bedrock/etc

E depois execute:
> nano bedrock.conf

E procure pela linha 
> default = {distro antes do bedrock}:/sbin/init

No meu caso está assim, porque eu tinha instalado o fedora antes.

![enter image description here](https://cdn.discordapp.com/attachments/829041782603120650/830181109991604294/unknown.png)

Como eu quero o init system runit, do void, eu irei trocar a opção "fedora"
por "void" e irei dar reboot. Para voltar ao antigo init system (Geralmente systemd) basta ir na mesma config file e trocar o nome da distro que você colocou, pelo nome da distro que foi instalada antes do bedrock

E pronto! Temos uma instalação ótima do BedRock Linux! Espero que tenham gostado do artigo, compartilhem para que outras pessoas conheçam o blog também, e entre no nosso [server do discord](https://discord.gg/2MdHyUzNYW) e no [grupo do telegram](https://t.me/joinchat/WUabhyL0jrYRHGzE)

