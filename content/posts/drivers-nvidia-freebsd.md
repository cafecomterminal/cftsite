---
title: "Drivers NVIDIA no FreeBSD"
date: 2021-04-06T16:12:08-03:00
draft: false
author: Drack
tags: ["BSD", "FreeBSD"]
cover: https://cdn.discordapp.com/attachments/829041782603120650/829081611723210793/nvidia-freebsd.png
---

A NVIDIA é uma marca famosa por ser uma grande produtora de placas de vídeo e GPUs. Suas placas são muito usadas por pessoas que buscam por um desempenho maior em jogos ou até mesmo em ferramentas que utilizam a aceleração gráfica. Porém, seus drivers são proprietários e são de difícil compatibilidade para sistemas Linux e BSD. Nesse artigo, iremos ensiná-los a como instalar os drivers NVIDIA em seu FreeBSD, um sistema operacional baseado no OS do "capeta" mais famoso no mundo da tecnologia!

![](https://cdn.discordapp.com/attachments/829041782603120650/829081611723210793/nvidia-freebsd.png)

Se por acaso você estiver seguindo o manual de configuração do Xorg, recomendamos ignorar o que o manual diz sobre a execução do Xorg-configure, e não o execute.

Se você já seguiu o manual e criou um xorg.conf, certifique-se de remover /etc/X11/xorg.conf ou /usr/local/etc/X11/xorg.conf antes de prosseguir:
```
$ rm -f /etc/X11/xorg.conf /usr/local/etc/X11/xorg.conf
```
## Instalando os drivers NVIDIA
Vamos dividir essa instalação em 6 passos, afim de uma melhor compreensão. Começamos assim, o primeiro passo!

### 1 - Instalando pacotes

Instale o pacote x11/nvidia-driver
```
$ pkg install nvidia-driver
```
Para algumas placas mais antigas, você precisará instalar os pacotes x11/nvidia-driver-340 ou x11/nvidia-driver-304.

Recomendamos fortemente que consulte a página de download da NVIDIA para ver qual versão do driver você precisa. Observe que não há necessidade (e é até contraproducente) baixar o driver dessa página.

### 2 - Carregando módulos do kernel na inicialização
Se a sua versão de driver for maior ou igual a 358.009, rode esse comando:
```
$  sysrc kld_list+="nvidia-modeset"
```
Caso contrátio, o nvidia-modeset está disponível apenas para versões de driver >= 358.009, se você usar uma versão mais antiga, rode esse comando:
```
$ sysrc kld_list+="nvidia"
```
Fazemos isso para adicionar uma entrada ao /etc/rc.conf, dessa forma poderemos carregar os módulos do kernel na inicialização.


### 3 - Carregando os módulos na seção atual
Nesse caso, você tem duas opções: reinicializar com: 
```
$ shutdown -r now
```
Ou carregar os módulos de kernel necessários agora com 
```
$ kldload nvidia-modeset
```
ou dessa forma se não houver modeset:
```
$ kldload nvidia
```

### 4 - Criando diretório de configuração
Essa parte é bem simples, basta criar o diretório com o seguinte comando:
```
$ mkdir -p /usr/local/etc/X11/xorg.conf.d
```
### 5 - Configure
Agora a parte definitiva e final, use seu editor de texto no diretório /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf, daremos exemplo utilizando o nano:
```
$ sudo nano /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf
```
Agora que estamos dentro do arquivo de configuração, cole isso nele:
```
Section "Device"
        Identifier "NVIDIA Card"
        VendorName "NVIDIA Corporation"
        Driver "nvidia"
EndSection
```
Salve o arquivo, e siga para o próximo passo...

### 6 - ...Reinicialize
Sim! Está tudo pronto! Reincializando todas as mudanças serão aplicadas e o que foi configurado para a inicialização deverá carregar normalmente.

Existe mais um passo bônus...
## Compartilhe!
E assim, chegamos ao fim de mais um artigo! Espero que vocês tenham gostado desse. Se puderem deixar o feedback ou compartilhar para seus amigos, estaremos agradecidos! Temos um [servidor no discord](https://discord.gg/2MdHyUzNYW), e também um [canal e um grupo no telegram](https://t.me/cafecomterminal), onde estaremos dispostos a ajudar! Lembrem-se de ler as regras da comunidade, ela está aberta para todos! Bons códigos, e até mais!
